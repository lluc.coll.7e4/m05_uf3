import java.util.Collections;

public class Conill extends Animal{
    @Override
    public String toString() { return "\uD83D\uDC30"; } // icona de conill

    public void mou() {
        Bloc[][] tauler = ConillsVsLlopsSim.tauler;
        if (energia == 0) {
            mor();                      // eliminem les referències a aquest objecte i confiem en el
            tauler[x][y].delAnimal();   // garbage collector per a destruir-lo (ja que no té referències)
        } else {
            int i = (int) (Math.random() * 3) - 1 + x;
            int j = (int) (Math.random() * 3) - 1 + y;
            boolean dinsTauler = i >= 0 && i < tauler.length && j >= 0 && j < tauler[0].length;
            if (dinsTauler && !tauler[i][j].esAigua() && !tauler[i][j].esLlop()) {
                if (tauler[i][j].esGespa()) {
                    energia = 10;
                }
                if (tauler[i][j].esRoca() && tauler[i][j].esConill()) {
                    if (tauler[i][j].getAnimal().getEnergia() < 2) {
                        int a = (int) (Math.random() * 3) - 1 + x;
                        int b = (int) (Math.random() * 3) - 1 + y;
                        ConillsVsLlopsSim.tauler[a][b] = new Bloc(tauler[a][b].getTerreny(), new Conill(a, b));
                        ConillsVsLlopsSim.totalAnimals++;
                    }
                } else {
                    tauler[x][y].delAnimal();
                    tauler[i][j].setAnimal(this);
                    x = i;
                    y = j;
                    energia--;
                }
            }
        }
    }

    /**
     * @param x posicio x
     * @param y posicio y
     *          energia posada a 10
     */
    public Conill(int x, int y){ super(x, y, 10); } // reutilitzem el constructor de la classe mare
}
